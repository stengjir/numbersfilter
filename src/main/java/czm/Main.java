package main.java.czm;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {
        Scanner sc = new Scanner(System.in);
        System.out.println(
                "Zadejte jmeno souboru pro cteni a nebo stisknete enter, pokud chcete cist ze souboru input.txt:");
        String f = sc.nextLine();
        if (f.equals("")) {
            System.out.println("Jmeno souboru nezadano, ctu soubor input.txt");
            f = "input.txt";
        }
        BufferedReader bfr = new BufferedReader(new FileReader(f));
        String s = bfr.readLine();
        ArrayList<Integer> arrayList = new ArrayList<>();
        do {
            int p = parseInt(s);
            if (p > 0) {
                System.out.println(p);
                arrayList.add(p);
            }
            s = bfr.readLine();
        } while (s != null);
        System.out.println("Prejete si ulozit profiltrovana cisla take do souboru y/n?");
        String a = sc.nextLine();
        if (a.equals("y")) {
            System.out.println("Zadejte jmeno souboru pro ulozeni cisel, nebo dejte enter pro ulozeni do output.txt");
            f = sc.nextLine();
            if (f.equals("")) {
                f = "output.txt";
            }
            saveNumbersToFile(arrayList, f);
            System.out.println("Cisla ulozena do souboru " + f + " dekuji za pouziti");
        } else {
            System.out.println("Dekuji za pouziti");
        }
    }

    static int parseInt(String s) {
        return Integer.parseInt(s);
    }

    static void saveNumbersToFile(ArrayList<Integer> numbers, String f) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter("output.txt"));
        for (int i = 0; i < numbers.size(); i++) {
            writer.write(String.valueOf(numbers.get(i)) + "\n");
        }
        writer.flush();
    }
}